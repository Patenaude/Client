﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class NetworkClient : MonoBehaviour
{
	int serverPort = 11111;
	public int port = 11111;
	//public string serverAddress = "127.0.0.1";

	public Button button;
	public Text text;
	public InputField input;

	int clientSocket = -1;
	int clientConnection = -1;
	byte channelID;
	bool initialized = false;
	bool connected = false;
	bool connecting = false;

	public string nom = "XYZ";

	public string separateur = "/~/";

	void Start()
	{
		GlobalConfig gc = new GlobalConfig();
		gc.ReactorModel = ReactorModel.FixRateReactor;
		gc.ThreadAwakeTimeout = 10;

		ConnectionConfig cc = new ConnectionConfig();
		channelID = cc.AddChannel(QosType.ReliableSequenced);

		HostTopology ht = new HostTopology(cc, 1);

		NetworkTransport.Init(gc);

		clientSocket = NetworkTransport.AddHost(ht, port);

		if (clientSocket < 0) { text.text += "Client socket creation failed!\n"; }
		else { text.text += "Client socket creation successful!\n"; }

		initialized = true;

		button.onClick = new Button.ButtonClickedEvent();
		button.onClick.AddListener(Connect);
	}

	void Connect()
	{
		// Connect to server
		byte error;
		clientConnection = NetworkTransport.Connect(clientSocket, input.text, serverPort, 0, out error);

		LogNetworkError(error);
	}

	void Disconnect()
	{
		byte error;
		NetworkTransport.Disconnect(clientSocket, clientConnection, out error);

		LogNetworkError(error);
	}

	void SendTestData()
	{
		// Send the server a message
		byte error;
		byte[] buffer = new byte[1024];
		Stream stream = new MemoryStream(buffer);
		BinaryFormatter f = new BinaryFormatter();
		f.Serialize(stream, "Hello!");

		NetworkTransport.Send(clientSocket, clientConnection, channelID, buffer, (int)stream.Position, out error);

		text.text += "Message sent\n";
		LogNetworkError(error);

		button.GetComponentInChildren<Text>().text = "Disconnect";
		button.onClick = new Button.ButtonClickedEvent();
		button.onClick.AddListener(Disconnect);
	}

	void SendData(string message)
	{
		byte error;
		byte[] buffer = new byte[1024];
		Stream stream = new MemoryStream(buffer);
		BinaryFormatter f = new BinaryFormatter();
		f.Serialize(stream, message);

		NetworkTransport.Send(clientSocket, clientConnection, channelID, buffer, (int)stream.Position, out error);

		text.text += "Message sent\n";
		LogNetworkError(error);
	}

	/// <summary>
	/// Log any network errors to the console.
	/// </summary>
	/// <param name="error">Error.</param>
	void LogNetworkError(byte error)
	{
		if (error != (byte)NetworkError.Ok)
		{
			NetworkError nerror = (NetworkError)error;
			text.text += "Error: " + nerror.ToString() + "\n";
		}
	}


	// Update is called once per frame
	void Update()
	{

		if (!initialized)
		{
			return;
		}

		int recHostId;
		int connectionId;
		int channelId;
		int dataSize;
		byte[] buffer = new byte[1024];
		byte error;

		NetworkEventType networkEvent = NetworkEventType.DataEvent;

		do
		{
			networkEvent = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, buffer, 1024, out dataSize, out error);

			switch (networkEvent)
			{
				case NetworkEventType.Nothing:
					break;
				case NetworkEventType.ConnectEvent:
					if (recHostId == clientSocket)
					{
						text.text += "Client: Client connected to " + connectionId.ToString() + "!\n";

						connected = true;
						button.GetComponentInChildren<Text>().text = "Send message";
						button.onClick = new Button.ButtonClickedEvent();
						button.onClick.AddListener(SendTestData);
					}

					break;

				case NetworkEventType.DataEvent:
					text.text += "Client: Received Data from " + connectionId.ToString() + "!\n";
					Stream stream = new MemoryStream(buffer);
					BinaryFormatter f = new BinaryFormatter();
					string msg = f.Deserialize(stream).ToString();
					TraiterMessage(msg);
					break;

				case NetworkEventType.DisconnectEvent:
					// Client received disconnect event
					if (recHostId == clientSocket && connectionId == clientConnection)
					{
						text.text += "Client: Disconnected from server!\n";

						connected = false;

						button.GetComponentInChildren<Text>().text = "Connect";
						button.onClick = new Button.ButtonClickedEvent();
						button.onClick.AddListener(Connect);
					}
					break;
			}

		} while (networkEvent != NetworkEventType.Nothing);
	}

	private void TraiterMessage(string message)
	{
		string[] parametres = message.Split(new string[] { separateur }, StringSplitOptions.RemoveEmptyEntries);

		if (parametres[0] == "GetInfoJoueur")
		{
			SendData("InfoJoueur" + separateur + nom);
		}
		else if (parametres[0] == "PartieStarted")
		{
			//modifier la scene
		}
		else if (parametres[0] == "CartesBlanche")
		{
			
		}
		//PartieStarted
		//CartesBlanches
		//CarteNoire
		//Czar
		//Reponses
		//InfoJoueurs
		//Message
		//Pret
		//Erreur
	}

}